"""
Фреймы и серии
"""
import pandas as pd
import pathlib as pl
population = pd.Series([17.04, 143.5, 9.5, 9.5, 45.5])

df = pd.DataFrame(
    {
        'country': ['Kazakhstan', 'Russia', 'Russia', 'Belarus', 'Ukraine'],
        'population': population,
        'square': [2724902, 17125191, 207600, 207600, 603628],
    }
)

# Создание новой колонки с результатом предыдущей
df['density'] = df['population'] / df['square'] * 1000000
print(df)
print('-' * 99)

# Гуппировка по country с суммой выборочных колонок
bdr = df.groupby(['country'])[['population', 'square']].sum().max()
print(bdr)
print('-' * 99)

# Гуппировка по country с суммой оставшихся колонок
bdr2 = df.groupby(['country']).apply(sum)
print(bdr2)
print('-' * 99)

# Гуппировка по country с суммой выборочных колонок. И последующим максимумом из них
bdr3 = df.groupby(['country'])[['population', 'square']].sum().max()
print(bdr3)
print('-' * 99)

# Сводная таблица
pvt = df.pivot_table(columns=['country'], values='population', aggfunc='sum')
print(pvt)
print('-' * 99)