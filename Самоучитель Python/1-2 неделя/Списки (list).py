#List(список) - упорядоченная последовательность, которую можно изменять. Допускаются одинаковые элементы.
#Tuple(кортеж) - последовательность, которая упорядочнена, но не изменяемая. Допускаются одинаковые элементы.
#Set(множество) - Неупорядоченная изменяемая последовательность. Одинаковые элементы удаляются.
#Dict(словарь) - неупорядочненная изменяемая последовательность, состоящая из пар ключ, значение. Ключи не дублируются.

#Списки
mkskom = ['Python', '1С', 'Java']
print(mkskom)
print(mkskom[0])
mkskom[2] = 'JavaScript'
print(mkskom)

#Интерация по списку
for x in mkskom:
    print(x)

#Длина списка
print(len(mkskom))

#Добавление эелемента
mkskom.append('Java')
print(mkskom)
for x in mkskom:
    print(x)
print(len(mkskom))

#Вставка элемента по позиции в списке
mkskom.insert(1,'C++')
print(mkskom)

#Удаление элемента
mkskom.remove('C++')
print(mkskom)

#Удаление элемента по индексу
last_element = mkskom.pop() #Удалил последнйи элемент Java
print(mkskom)
print(last_element) #Вывел последний элемент, но он же вроде как удалился? Почему он вывелся?

del mkskom[2]
print(mkskom)

mkskom.clear()
print(mkskom)

# del mkskom
# print(mkskom) #Будет ошибка, так как список удален

mkskom = list(('Python', '1C', 'Java'))
print(mkskom)

mkskom.append('C++')
mkskom.insert(2, 'JavaScript')
print(mkskom)
mkskom.reverse() #Развернул список
print(mkskom)
mkskom.sort() #Сортировка по алфавиту
print(mkskom)

fruit = list()
print(fruit)
fruit = ['Яблоко', 'Банан', 'Дыня', 'Апельсин']
print(fruit)
fruit.append('Персик')
fruit.append('Ананас')
print(fruit)

random = []
random.append(True)
random.append(100)
random.append(1.1)
random.append('МКСКОМ')
print(random)

print(len(random))
print(random[0])
print(random[1])
print(random[2])
print(random[3])

random[2] = 'Python'
print(random)

allin = fruit + random
print(allin)

allin.pop()
print(allin)
allin.pop()
print(allin)

print(len(allin))

#language = ['Python', '1С', 'Java']
#guess = input('Угадай язык программирования:')
#if guess in language:
#    print('Ты угадал!')
#else:
#    print('Неправильно! Попробуй еще раз!')

#Хранение списка внутри списка
music = []
rap = ['Баста',
       'Кравц']
rock = ['Ария',
        'Кино']
djs = ['Tiesto']
music.append(rap)
music.append(rock)
music.append(djs)
print(music)

rap = music[0]
rap.append('Ченая Экономика')
print(rap)
print(music)

#Хранение кортежа внутри списка
location = []
tula = (54.1960, 37.6182)
moscow = (55.7522, 37.6155)
location.append(tula)
location.append(moscow)
print(location)

eights = ['Эдгар Аллан По',
          'Чарльз Диккенс']
nines = ['Хемингуэй',
         'Фицджеральд',
         'Оруэлл']
authors = (eights, nines)
print(authors)
bdays = {'Хемингуэй':
         '21.07.1899',
         'Фицджеральд':
         '24.09.1896'}
my_list = [bdays]
print(my_list)
my_tuple = (bdays, )
print(my_tuple)

ru = {'Расположение': #кортеж
          (55.7522,
           37.6155),

      'знаменитости': #список
      ['Андрей Звягинцев',
       'Юрий Быков',
       'Петр Буслов'],

      'факты': #словарь
          {'город':
           'Москва',
           'страна':
           'Россия'}
}
print(ru)

my_str = 'barbarian'
print('bar' in my_str)