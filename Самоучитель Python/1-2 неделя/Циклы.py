#Цикл while
i = 5
while i < 29:
    print(i)
    i = i + 2

#Цикл for
p = 'Python Programmer'
for i in p:
    print(i * 2, end='')
print(p)

#Оператор Continue
d = 'Python Developer'
for y in d:
    if y == 'P':
        continue
    print(y * 2, end='')
print(d)

#Оператор Break
for i in 'Hello Python':
    if i == 'o':
        break
    print(i * 2, end='')

#else
for i in 'hello python':
    if i == 'a':
        break
else:
    print('Буквы а нет')

name = 'Владислав'
for character in name:
    print(character)

shows = ['Во все тяжкие',
         'Секретные материалы',
         'Фарго']
for show in shows:
    print(show)

coms = ('Теория большого взрыва',
        'Друзья')
for show in coms:
    print(show)

people = {'Джим Парсонс':
          'Теория большого взрыва',
          'Брайан Крэнстон':
          'Во все тяжкие',
          'Екатерина Старшова':
          'Папины Дочки'
}
for character in people:
    print(character)

tv = ['Во все тяжкие', #Создается писок сериалов
      'Секретные материалы',
      'Фарго ']
i = 0 #задается переменная "Индекса" с 0, так как индекс первого элемента = 0
for show in tv: #задаю цикл
    new = tv[i] #создается переменная, в которую пихаются элементы по индексу
    new = new.upper() #делаю элементы заглавными буквами
    tv[i] = new #помещаю все индексируемые элементы в переменную new
    i += 1 #указываю как увеличивается
print(tv) #Вывожу

tv = ['Во все тяжкие', #Создаю список ТВ
      'Секретные материалы',
      'Фарго']
coms = ['Теория большого взрыва', #Создаю список КОМС
        'Друзья',
        'Папины дочки']
all_shows = [] #Создаю пустой список
for show in tv: #Создаю цикл, который перебирает все элементы списка ТВ и помещает их в список ОЛШОУС, делаю все буквы заглавными
    show  = show.upper()
    all_shows.append(show)
for show in coms: #Создаю цикл, который перебирает все элементы списка КОМС и помещает их в список ОЛШОУС, делаю все буквы заглавными
    show = show.upper()
    all_shows.append(show)
print(all_shows) #Вывожу заполненный новый список ОЛШОУС

for i in range (1, 10):
    print(i)

mkskom = ['Pyhon', '1C', 'Java']
for i in mkskom:
    print(i)

x = 10
while x > 0:
    print('{}'.format(x))
    x -= 1
print('Python')

#qs = ['Как тебя зовут?', #Создаю список с вопросами и пихаю в переменную qs
#      'Твой любимый цвет?',
#      'Что ты делаешь?']
#n = 0 #Создаю переменную n и присваиваю ей значение 0
#while True:
#    print('Введи X для выхода')
#    a = input(qs[n]) #Указываю ввод ответов
#    if a == 'X': #Указываю, что если ввести X, то программа перестает работать
#        break
#    n = (n + 1) % 3 #Указываю сколько проходить по циклу. 1 делить по модулю на 3, будет 1 и т.д. 3 % 3 = 0.

for i in range(1, 6):
    if i == 3:
        continue
    print(i)

i = 1
while i <= 5:
    if i == 3:
        i += 1
        continue
    print(i)
    i += 1

for i in range(1,3):
    print(i)
    for letter in ['а', 'б', 'в']:
        print(letter)

list1 =[1, 2, 3, 4]
list2 = [5, 6, 7, 8]
aded = []
for i in list1:
    for j in list2:
        aded.append(i + j)
print(aded)