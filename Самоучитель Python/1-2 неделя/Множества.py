#Множества - неупорядочненная и не индексируемая последовательность.
mkskom = {'Python', '1C' , 'Java'}
print(mkskom) #Элементы в множестве не упорядочнены
mkskom = {'Python', '1C', 'Java', '1C'}
print(mkskom)
for x in mkskom:
    print(x)
#Добавление одного элемента во множество
mkskom.add('JavaScript')
print(mkskom)
#Добавление нескольких элементов
mkskom.update(['GO', 'PHP'])
for x in mkskom:
    print(x)
print(mkskom)

#Длина, кол-во элементов
print(len(mkskom))

#Удаление элементов
mkskom.remove('PHP') #Удалит элемент, но если элемента нет, то выдаст ошибку
mkskom.discard('GO') #Удалит элемент, если он есть.
print(mkskom)

#Можно ли удалить одной операцией несколько элементов? Уточнить.

x = mkskom.pop()
print(x)
print(mkskom)

mkskom.clear()
print(mkskom)

#del mkskom
#print(mkskom) #Будет ошибка, так как множества не существует.

mkskom = set(('Python', '1C', 'Java'))
lang = {'Python', 'Go', 'C++'}
print(mkskom)
print(lang)

mkskom.add('Javascript')
lang.add('C#')
print(mkskom)
print(lang)

print(mkskom.difference(lang))
print(mkskom.intersection(lang))
print(mkskom.isdisjoint(lang))
print(mkskom.symmetric_difference(lang))

print(mkskom.union(lang))
mkskom.update(lang)
print(mkskom)
print(lang)
del lang
