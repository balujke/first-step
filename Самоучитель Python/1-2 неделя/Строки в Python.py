#Просмотр по инлексу
a = 'Привет, МКСКОМ! '
print (a[8])
print (a[8:14])
#Метод strip() - удаляет любые пробелы с начала или конца строки
print (a.strip())
#Метод len() - возращает длину строки
print (len(a))
#Метод lower() - возращает строку в нижнем регистре
print (a.lower())
#Метод upper() - возращает строку в верхнем регистре
print (a.upper())
#Метод replace(x, y) - заменяет часть строки x на строку y
print (a.replace('МКСКОМ', 'Python'))
print (len(a))
print (a)
# Получается он только в выводе заменяет слово, в самой переменной отсается МКСКОМ
#Метод split (x) - разбивает строку на список из подстроки, по резделителю x
print(a.split(','))

c = '''Владислав
Сахарук
Будущий
разработчик'''
print(c)

q = 'Vladislav '
w = 'Sakharuk'
print(q + w)
print(q * 3)
print(q[2:6])
print(q[::-1])

q = 'pythoN developer'
print(q.title())
print(q.swapcase())

author = 'Лавкрафт'
print(author[0])
print(author[1])
print(author[2])
print(author[3])
print(author[4])
print(author[5])
print(author[6])
print(author[7])
print(author[-1])

ff = 'Ф. Фицджеральд'
print(ff)
ff = 'Ф. Скотт Фицджеральд'
print(ff)

a = 'Кот '
b = 'в '
c = 'сапогах'
d = a + b + c
print(d)
print(d.upper())

a = 'Я смогу стать разработчиком. Точно-точно.'
print(a.split('.'))

firstfree = 'абв'
result = '+'.join(firstfree)
print(result)

words = ['Рыжая',
         'лисица',
         'сделала',
         'кувырок',
         'через',
         'голову',
         '.']
one = ''.join(words)
print(one)
one = ' '.join(words)
print(one)

city = '     Moscow     '
print(city)
city = city.strip()
print(city)
city = city.replace('w', 'vv')
print(city)

city = 'Moscow is a capital\n of Russia'
print(city)

fict = ['Толстой',
        'Дик',
        'Оруэлл',
        'Пелевин',
        'Остин']
print(fict[0:3])

ivan = '''Петр Иванович успокоился и с интересом стал расспрашивать подробности о кончине Ивана Ильича'''
print(ivan[0:24])
print(ivan[24:93])