#Словарь - неупорядочненная последовательность, гибким к изменениям и индексированным. Ключ + Значение.

# thisdict = {
#     'brand' : 'VolksWagen',
#     'model' : 'Polo',
#     'year' : 2021
# }
# print(thisdict)
#
# x = thisdict['model']
# print(x)
#
# x = thisdict.get('brand')
# print(x)
#
# thisdict['year'] = 2020
# x = thisdict.get('year')
# print(x)
# print(thisdict)
#
# #Цикл FOR по словарю
# for x in thisdict:
#     print(x)
#
# for x in thisdict:
#     print(thisdict[x])
#
# for x in thisdict.values():
#     print(x)
#
# for x, y in thisdict.items():
#     print(x, y)
#
# #Длина словаря
# print(len(thisdict))
#
# #Добавление элементов
# thisdict['color'] = 'Brown'
# print(thisdict)
#
# for x, y in thisdict.items():
#     print(x, y)
#
# #Удаление элементов
# thisdict.pop('year')
# print(thisdict)
#
# thisdict.popitem()
# print(thisdict)
#
# thisdict.clear()
# print(thisdict)
#
# #Словарь с помощью конструктора dict
# thisdict = dict(brand='VolskWagen', model='Polo', year=2020, color='Brown')
# print(thisdict)
#
# for x, y in thisdict.items():
#     print(x, y)
#
# print(thisdict.values())
# print(thisdict.keys())
# print(thisdict.items())
# print(thisdict.get('model'))
#
# fruits = {'Яблоко':
#           'Красное',
#           'Банан':
#           'Желтый'}
# print(fruits)
#
# bill = {'Билл Гейтс' : 'Щедрый'}
# print('Билл Гейтс' in bill)
#
# rhymes = {'1' : 'смех',
#           '2' : 'синий',
#           '3' : 'я',
#           '4' : 'этаж',
#           '5' : 'жизнь'
# }
# n = input('Введите число: ')
# if n in rhymes:
#     rhyme = rhymes[n]
#     print(rhyme)
# else:
#     print('Не найдено.')

# mydict = {}
# value = mydict.get(key, default_value)
# mydict['not there']

