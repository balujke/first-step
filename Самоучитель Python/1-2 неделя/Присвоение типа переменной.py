# int(): пример преобраования
q = int (1)
w = int (2.8)
e = int ('3')

print (q + w + e)
print (q)
print (w)
print (e)

# float: пример преобразования
q = float(1)
w = float(2.8)
e = float('3')
r = float('4.2')

print (q + w + e + r)
print (q)
print (w)
print (e)
print (r)

# str(): пример преобразования
q = str('s1')
w = str(2)
e = str(3.0)
r = (' ')

print (q + r + w + r + e)
print (q)
print (w)
print (e)