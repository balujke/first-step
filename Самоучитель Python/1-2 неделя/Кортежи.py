#Кортеж - упорядочненная последовательность, неименяемая. Пишутся в круглых скобках.
mkskom = ('Python', '1C', 'Java')
print(mkskom)
print(mkskom[0])

#Итерация по кортежу
for x in mkskom:
    print(x)

print(len(mkskom))

del mkskom

mkskom = tuple(('1С', 'Python', 'Java'))
print(mkskom)

print(mkskom.index('Java')) #Возращает элемент по индексу
print(mkskom.count('1С')) #Возвращает кол-во раз, которое указанный элемент встречается в кортеже

rndm = ('М. Джексон', 1958, True)
print(rndm)

dys = ('1984',
       'О дивный новый мир',
       '451 градус по Фаренгейту')
print(dys[2])

print('1984' in dys)