def greet(name):
    print('Hello', name)

greet('Vladislav')
greet('Ivan')

def my_function (country='Росии'):
    print('Я из ' + country)
my_function()
my_function('Англии')
my_function('США')

def my_function (x):
    return 5 * x
print(my_function(3))
print(my_function(5))

#ef my_function ()

def has_o(string):
    return 'o' in string.lower()

l = ['one', 'two', 'three']

n1 = list(filter(has_o, l))
print(n1)