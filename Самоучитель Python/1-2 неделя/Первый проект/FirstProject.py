"""
Первый проект. Кейс 1

Написать функцию, которая принимает на вход сколько угодно параметров, находит среди этих параметров,
все которые начинаются со слова "sum".

На выход возвращает все найденные параметры, а также сумму значений этих параметров.
"""

from collections import OrderedDict

my_dict = {
    'Sum41': '10.6688989',
    'Sum51': '10,76',
    'rock': 15,
    'Sums56': 55.04,
    'Sumsums': 100,
    'Sumss': '+175',
    'sumans': '-166',
    'bum2': '43',
    'bum': 22,
}

my_dict1 = {
    'Sum41': 20,
    'Sum56': 30,
    'Rocker': 10
}

DEF_REPL_KEYS = OrderedDict({
    ' ': '',
    '.': '',
    ',': '',
    '+': '',
    '-': ''
})


def sum_func(num_of_digit=2, key_found='sum', **kwargs):
    """
    Принимает на вход сколько угодно параметров, находит среди этих параметров,
    все которые начинаются со слова "sum"
    """

    res_func = {}

    new_dict = {}
    for k, v in kwargs.items():
        if k in ['num_of_digit', 'key_found']:
            continue

        key_compare = k.lower().startswith(key_found)
        if key_compare and type(v) == int:
            new_dict[k] = float(v)

        elif key_compare and type(v) == float:
            new_dict[k] = float(v)

        elif key_compare and type(v) == str:
            isd = _combine_value_to_compare(v).isdigit()

            if v.replace(' ', '').isdigit():
                new_dict[k] = float(v.replace(' ', ''))
            if isd:
                new_dict[k] = float(v.replace(' ', '').replace(',', '.'))

            elif isd:
                continue

    res_func['found_value'] = new_dict
    res_func['total_sum'] = round(sum(new_dict.values()), num_of_digit)

    return res_func


def _combine_value_to_compare(v):
    """
    Универсальная замена лишних символов для поиска
    числа флоат в строке
    """

    for key, val in DEF_REPL_KEYS.items():
        v = v.replace(key, val)
    return v


if __name__ == '__main__':
    res = sum_func(**my_dict, num_of_digit=4)
    print('Общая сумма: ', res['total_sum'])

    res = sum_func(**my_dict, num_of_digit=2)
    print('Общая сумма: ', res['total_sum'])

    res = sum_func(**my_dict, num_of_digit=2, key_found='bum')
    print('Общая сумма bum: ', res['total_sum'])

    res = sum_func(**my_dict1)
    print('Общая сумма: ', res['total_sum'])
