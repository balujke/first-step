#Типы числа
q = 1 # int
w = 2.8 # float
e = 1j # complex

print (type(q))
print (type(w))
print (type(e))

a = 1
s = 35656222554887711
d = -3255522
print (type(a))
print (type(s))
print (type(d))

z = 1.10
x = 1.0
c = -35.59

print (type(z))
print (type(x))
print (type(c))

p = 35e3
o = 12e4
i = -87.7e100

print (type(p))
print (type(o))
print (type(i))

l = 3 + 5j
k = 5j
j = -5j

print (type(l))
print (type(k))
print (type(j))

a = 5
print(-a)
print(abs(a))

a = 5
b = 13
print(a|b)
print(a^b)
print(a&b)
print(a<<b)
print(a>>b)
print(~a)

q = -37
print(bin(q))

