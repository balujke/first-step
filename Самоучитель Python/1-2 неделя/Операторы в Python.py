#Арифмитические операторы
print(4 + 5)
print(5-4)
print(100 * 1000)
print(10 / 5)
print(8 % 3)
print(8 ** 2)
print(15 // 4)

#Операторы присвоения
x = 5
print(x) # x = 5
x += 3
print(x) # x = 5 + 3 = 8
x -= 1
print(x) # x = 8 - 1 = 7
x *= 3
print(x) # x = 7 * 3 = 21
x /= 3
print(int(x)) # x = 21 / 3 = 7
x %= 6
print (x)
x = 100
x //= 5
print (x) # x = 100 / 5 = 20
x **= 2
print(x) # = 20 в степени 2 = 400
x &= 4
print(x) #Не совсем понятно что тут (УТОЧНИТЬ)
x = 10
x |= 1
print(x) #Не совсем понятно что тут (УТОЧНИТЬ)
x ^= 3
print(x) #Не совсем понятно что тут (УТОЧНИТЬ)
x >>= 3
print(x) #Не совсем понятно что тут (УТОЧНИТЬ)
x <<= 3
print(x) #Не совсем понятно что тут (УТОЧНИТЬ)

#Операторы сравнения
x = 4
y = 5
print(x == y)
print(x != y)
print(x > y)
print (x < y)
print(x >= y)
print(x <= y)

#Логические операторы
x = 5
y = 4
print(x <= 5 and y < 10)
print(x < 5 or y < 4)
print(not(x > 5 and y < 4))

#Операторы тождественности
x = 5
y = 5
print(x is y)
print(x is not y)

