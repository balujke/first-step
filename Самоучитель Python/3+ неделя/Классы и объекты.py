"""
Классы и объекты
"""

class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age


p1 = Person("Василий",  36)
print(p1.name)
print(p1.age)


class Person:
    def __init__(mysillyobject, name, age):
        mysillyobject.name = name
        mysillyobject.age = age

    def myfunc(abc):
        print("Привет, меня зовут " + abc.name)


p1 = Person("Василий", 36)
p1.myfunc()