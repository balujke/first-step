# import os
# os.path.join('Users',
#              'bob',
#              'st.txt')
# st = open('st.txt', 'w')
# st.write('Привет от Python')
# st.close()
#
# with open('st.txt', 'w') as f:
#     f.write('привет от Python!')
#
# with open('st.txt', 'r') as f:
#     print(f.read())
#
# my_list = list()
#
# with open('st.txt', 'r') as f:
#     my_list.append(f.read())
# print(my_list)
#
# import csv
#
# with open('st.csv', 'w') as f:
#     w = csv.writer(f, delimiter=',')
#     w.writerow(['один',
#                 'два',
#                 'три'])
#     w.writerow(['четыре',
#                 'пять',
#                 'шесть'])
#
# import csv
#
# with open('st.csv', 'r') as f:
#     r = csv.reader(f, delimiter=',')
#     for row in r:
#         print(','.join(row))

# f = open ('C:\\Users\\v.saharuk\\Desktop\\Обучение Python\\files\\myfile.txt', 'r')
# firstline = f.readline()
# secondline = f.readline()
# print (firstline)
# print (secondline)
# f.close()
#
# f = open ('C:\\Users\\v.saharuk\\Desktop\\Обучение Python\\files\\myfile.txt', 'r')
# for line in f:
#     print(line, end='')
# f.close()
#
# f = open ('C:\\Users\\v.saharuk\\Desktop\\Обучение Python\\files\\myfile.txt', 'a')
#
# f.write('\nThis sentence will be append.')
# f.write('\nPython is Fun!')
#
# f.close()

# inputFile = open('C:\\Users\\v.saharuk\\Desktop\\Обучение Python\\files\\myimage.jpg', 'rb')
# outputFile = open('C:\\Users\\v.saharuk\\Desktop\\Обучение Python\\files\\myoutputimage.jpg', 'wb')
#
# msg = inputFile.read(10)
#
# while len(msg):
#     outputFile.write(msg)
#     msg = inputFile.read(10)
# inputFile.close()
# outputFile.close()

fout = open('oops.txt', 'wt')
print('Oops, I created a file.', file=fout)
fout.close()

poem = '''There was a young lady named Bright,
Whose speed was far faster than light;
She started one day
In a relative way,
And returned on the previous night.'''
print(len(poem))

fout = open('relativity.txt', 'wt')
fout.write(poem)
fout.close()