"""
Массивы
"""

cars = ['Ford', 'Volvo', 'BMW']

cars[0] = 'Toyota'

# x = cars[0]

cars.append('Honda')

#cars.pop(1)

cars.extend(['VolksWagen'])

for x in cars:
    print(x)

print(cars.reverse())
