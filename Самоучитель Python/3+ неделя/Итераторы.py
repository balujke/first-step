"""
Итераторы
"""

mytuple = ("яблоко", "банан", "вишня")
myit = iter(mytuple)
print(next(myit))
print(next(myit))
print(next(myit))

mystr = "банан"
myit =  iter(mystr)
print(next(myit))
print(next(myit))
print(next(myit))
print(next(myit))
print(next(myit))

mytuple = ("яблоко", "банан", "вишня")
for x in mytuple:
    print(x)

mystr = "банан"
for x in mystr:
    print(x)